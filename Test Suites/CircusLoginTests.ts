<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CircusLoginTests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cfba1bab-1d35-4a90-9947-5a2f424eeb23</testSuiteGuid>
   <testCaseLink>
      <guid>487e66ee-498a-435d-948e-71b3cffe6d15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CircusAnonymous</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>241a9a02-510d-4321-91c4-c00ad7e5af6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CircusLogin</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
